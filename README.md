# EAW Remastered
EAW Remastered Repository

EAW Remastered - Providing users a visually remastered version of the game Empire at War whilst still authentic to the original game mechanics. This overhaul focuses on updating existing 3d assets alongside their animations, textures, and respective UI elements.

This passion project, was developed by James McGinty with the primary langauge being C mixing in LUA. C is primarily used to interact with existing game configs to implement new 3D assets, modify existing in-game units, and to overhaul the User Interface. LUA has been used for the addition of new quality of life scripts which are assigned to certain 3D assets to allow features such as Z-layer movement support and randomly spawning different visual variations of assets.

Please visit the [Project Gallery](https://imgur.com/a/JagEmih) for visual representation of the UI/UX work completed.
